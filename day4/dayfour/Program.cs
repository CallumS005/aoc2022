﻿/*

	From the test input, there should be 2 pairs within the data

*/

namespace dayfour
{
	internal class Program
	{
		private static List<string> input = new List<string>();

		private static void Main(string[] args)
		{
			ReadInput();
			Console.WriteLine(CalculateTotalOverlapping());
		}

		private static void ReadInput()
		{
			if (File.Exists("input.txt"))
				input = File.ReadAllLines("input.txt").ToList();
			else
				Console.Error.WriteLine("Unable to open input.txt");
		}

		// Checks if the elf pairs fully contain each other
		private static int CalculateTotalContainedInArea()
		{
			int total = 0;

			foreach (string inp in input)
			{
				string[] pair = inp.Split(",");
				int firstRangeLower = int.Parse(pair[0].Split("-")[0]);
				int firstRangeUpper = int.Parse(pair[0].Split("-")[1]);

				int secondRangeLower = int.Parse(pair[1].Split("-")[0]);
				int secondRangeUpper = int.Parse(pair[1].Split("-")[1]);

				if ((secondRangeLower >= firstRangeLower && secondRangeUpper <= firstRangeUpper) ||
					(firstRangeLower >= secondRangeLower && firstRangeUpper <= secondRangeUpper))
					total++;
			}

			return total;
		}

		private static int CalculateTotalOverlapping()
		{
			int total = 0;

			foreach (string inp in input)
			{
				string[] pair = inp.Split(",");
				int firstRangeLower = int.Parse(pair[0].Split("-")[0]);
				int firstRangeUpper = int.Parse(pair[0].Split("-")[1]);

				int secondRangeLower = int.Parse(pair[1].Split("-")[0]);
				int secondRangeUpper = int.Parse(pair[1].Split("-")[1]);

				if (firstRangeLower <= secondRangeUpper && firstRangeUpper >= secondRangeLower)
					total++;
			}

			return total;
		}
	}
}