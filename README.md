# Advent of Code 2022
Advent of code is a challange to complete puzzles in the 24 days leading up to Christmas. This year is the first year of doing it, and so far I am really enjoying it.

This year I will complete these puzzles using the 7 languages I am most confortable in:
- C++
- C#
- Java
- Javascript
- Lua
- Python
- Visual Basic

- [x] Day One
- [x] Day Two
- [x] Day Three
- [x] Day Four
- [ ] Day Five
- [ ] Day Six
- [ ] Day Seven
- [ ] Day Eight
- [ ] Day Nine
- [ ] Day Ten
- [ ] Day Eleven
- [ ] Day Twelve
- [ ] Day Therteen
- [ ] Day Fourteen
- [ ] Day Fifteen
- [ ] Day Sixteen
- [ ] Day Seventeen
- [ ] Day Eighteen
- [ ] Day Nineteen
- [ ] Day Twenty
- [ ] Day Twenty One
- [ ] Day Twenty Two
- [ ] Day Twenty Three
- [ ] Day Twenty Four
