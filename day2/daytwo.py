total = 0
input = []

"""
A = Rock
B = Paper
C = Scissors

X = Lose
Y = Draw
Z = Win

Rock = 1
Paper = 2
Scissors = 3

0 = Lost
3 = Draw
6 = Win
"""

def ReadInput():
	global input
	file = open("input.txt", "r")
	input = file.readlines()

def ConvertElfInput(inp):
	if inp == "A":
		return 1 # Returns rock
	elif inp == "B":
		return 2 # Returns paper
	elif inp == "C":
		return 3 # Returns scissors

def GetHumanInput(inp, elfInp):
	if inp == "X":
		if elfInp == 1:
			return 3
		elif elfInp == 2:
			return 1
		elif elfInp == 3:
			return 2
	elif inp == "Y":
		return elfInp
	elif inp == "Z":
		if elfInp == 1:
			return 2
		elif elfInp == 2:
			return 3
		elif elfInp == 3:
			return 1

def GetWinState(elf, human):
	if elf == human:
		return 3 # Returns draw
	else:
		if elf == 1 and human == 3:
			return 0 # Returns lose
		elif elf == 2 and human == 1:
			return 0 # Returns lose
		elif elf == 3 and human == 2:
			return 0 # Returns lose
		else:
			return 6 # Returns win

def Main():
	ReadInput()

	global total
	for i in input:
		# Input: Elf RequiredState
		inputGroup = i.split(" ")
		elf = ConvertElfInput(inputGroup[0])
		human = GetHumanInput(inputGroup[1].replace("\n", ""), elf)
		total = total + (GetWinState(elf, human) + human)

	print(total)

if __name__ == "__main__":
	Main()