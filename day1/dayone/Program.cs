﻿namespace dayone
{
	class Elf
	{
		public Elf(int id)
		{
			elfId = id;
		}

		public void CalculateTotal()
		{
			foreach (int item in itemCalories)
			{
				totalCalories += item;
			}
		}

		new public string ToString()
		{
			string toReturn = $"elfId: {elfId}\ntotalCalories: {totalCalories}\nitemCalories: \n";
			foreach (int item in itemCalories)
				toReturn += $"{item},";

			toReturn += "\n";

			return toReturn;
		}

		public List<int> itemCalories = new List<int>();
		public int totalCalories = 0;
		public int elfId = 0;
	}

	internal class Program
	{
		private static List<string> input = new List<string>();
		private static List<Elf> elfs = new List<Elf>();


		public static void Main(string[] args)
		{
			ReadInput();
			MakeElfs();
			// Part One
			Console.WriteLine("Elf with the max calories is: ");
			Console.WriteLine(FindElfWithMaxCalories().ToString());

			// Part Two
			Elf one = FindElfWithMaxCalories();
			elfs.Remove(one);
			Elf two = FindElfWithMaxCalories();
			elfs.Remove(two);
			Elf three = FindElfWithMaxCalories();
			elfs.Remove(three);

			Console.WriteLine($"The total of the top 3 elfs carrying the most calories is: {one.totalCalories + two.totalCalories + three.totalCalories}");
			Console.WriteLine($"Elf: {one.elfId}, {two.elfId}, {three.elfId}");
		}

		private static void ReadInput()
		{
			if (File.Exists("input.txt"))
				input = File.ReadAllLines("input.txt").ToList();
			else
				Console.Error.WriteLine("Unable to open input.txt");
		}

		private static void MakeElfs()
		{
			int count = 0;

			Elf currentElf = new Elf(count);
			elfs.Add(currentElf);

			foreach (string line in input)
			{
				if (line == "")
				{
					currentElf.CalculateTotal();
					count++;
					currentElf = new Elf(count);
					elfs.Add(currentElf);
				}
				else
				{
					try
					{
						currentElf.itemCalories.Add(int.Parse(line));
					}
					catch (System.Exception)
					{
						Console.Error.WriteLine($"Error parsing data: {line}");
						throw;
					}
				}
			}
		}

		private static Elf FindElfWithMaxCalories()
		{
			Elf maxElf = elfs[0];

			foreach (Elf elf in elfs)
				if (elf.totalCalories > maxElf.totalCalories)
					maxElf = elf;

			return maxElf;
		}
	}
}