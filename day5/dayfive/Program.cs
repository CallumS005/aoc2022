﻿namespace dayfive
{
	internal class Program
	{
		private static List<string> input = new List<string>();

		// This is the proper data
		private static Dictionary<int, List<string>> crates = new Dictionary<int, List<string>>()
		{
			/*
				[V]     [B]                     [C]
				[C]     [N] [G]         [W]     [P]
				[W]     [C] [Q] [S]     [C]     [M]
				[L]     [W] [B] [Z]     [F] [S] [V]
				[R]     [G] [H] [F] [P] [V] [M] [T]
				[M] [L] [R] [D] [L] [N] [P] [D] [W]
				[F] [Q] [S] [C] [G] [G] [Z] [P] [N]
				[Q] [D] [P] [L] [V] [D] [D] [C] [Z]
				 1   2   3   4   5   6   7   8   9 
			*/
			{ 1, new List<string>(){"Q", "F", "M", "R", "L", "W", "C", "V"} },
			{ 2, new List<string>(){"D", "Q", "L"} },
			{ 3, new List<string>(){"P", "S", "R", "G", "W", "C", "N", "B"} },
			{ 4, new List<string>(){"L", "C", "D", "H", "B", "Q", "G"} },
			{ 5, new List<string>(){"V", "G", "L", "F", "Z", "S"} },
			{ 6, new List<string>(){"D", "G", "N", "P"} },
			{ 7, new List<string>(){"D", "Z", "P", "V", "F", "C", "W"} },
			{ 8, new List<string>(){"C", "P", "D", "M", "S"} },
			{ 9, new List<string>(){"Z", "N", "W", "T", "V", "M", "P", "C"} },
		};

		// private static Dictionary<int, List<string>> crates = new Dictionary<int, List<string>>()
		// {
		// 	{1, new List<string>(){"Z", "N"} },
		// 	{2, new List<string>(){"M", "C", "D"} },
		// 	{3, new List<string>(){"P"} }
		// };

		private static void Main(string[] args)
		{
			ReadInput();

			PartTwo();

			for (int i = 1; i < crates.Count + 1; i++)
			{
				if (crates[i].Count < 1)
					continue;

				Console.Write(crates[i][crates[i].Count - 1]);
			}

			Console.WriteLine("");
		}

		private static void ReadInput()
		{
			if (File.Exists("input.txt"))
				input = File.ReadAllLines("input.txt").ToList();
			else
				Console.Error.WriteLine("Unable to open input.txt");
		}

		private static void PartOne()
		{
			foreach (string inst in input)
			{
				string[] groups = inst.Split(" ");

				int quantity = int.Parse(groups[1]);
				int from = int.Parse(groups[3]);
				int to = int.Parse(groups[5]);

				for (int i = 0; i < quantity; i++)
				{
					string crate = crates[from].Last();

					crates[to].Add(crate);
					crates[from].RemoveAt(crates[from].Count - 1);
				}
			}
		}

		private static void PartTwo()
		{

			DateTime first = DateTime.Now;

			foreach (string inst in input)
			{
				string[] groups = inst.Split(" ");

				int quantity = int.Parse(groups[1]);
				int from = int.Parse(groups[3]);
				int to = int.Parse(groups[5]);

				List<string> addBuffer = new List<string>();

				for (int i = 0; i < quantity; i++)
				{
					addBuffer.Add(crates[from].Last());
					crates[from].RemoveAt(crates[from].Count - 1);
				}

				for (int i = addBuffer.Count; i > 0; i--)
				{
					crates[to].Add(addBuffer[i - 1]);
				}
			}

			DateTime second = DateTime.Now;

			Console.WriteLine(second - first);
		}

	}
}