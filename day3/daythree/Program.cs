﻿namespace daythree
{
	internal class Program
	{
		private static List<string> input = new List<string>();

		static Dictionary<char, int> priorities = new Dictionary<char, int>()
		{
			{'a', 1},
			{'b', 2},
			{'c', 3},
			{'d', 4},
			{'e', 5},
			{'f', 6},
			{'g', 7},
			{'h', 8},
			{'i', 9},
			{'j', 10},
			{'k', 11},
			{'l', 12},
			{'m', 13},
			{'n', 14},
			{'o', 15},
			{'p', 16},
			{'q', 17},
			{'r', 18},
			{'s', 19},
			{'t', 20},
			{'u', 21},
			{'v', 22},
			{'w', 23},
			{'x', 24},
			{'y', 25},
			{'z', 26},

			{'A', 27},
			{'B', 28},
			{'C', 29},
			{'D', 30},
			{'E', 31},
			{'F', 32},
			{'G', 33},
			{'H', 34},
			{'I', 35},
			{'J', 36},
			{'K', 37},
			{'L', 38},
			{'M', 39},
			{'N', 40},
			{'O', 41},
			{'P', 42},
			{'Q', 43},
			{'R', 44},
			{'S', 45},
			{'T', 46},
			{'U', 47},
			{'V', 48},
			{'W', 49},
			{'X', 50},
			{'Y', 51},
			{'Z', 52},
		};

		static void Main(string[] args)
		{
			ReadInput();

			// Part One
			List<char> repeated = GetDuplicates();
			Console.WriteLine(CalculateTotal(repeated));

			List<char> badges = GetBadges();
			Console.WriteLine(CalculateTotal(badges));

			// Part Two
		}

		private static void ReadInput()
		{
			if (File.Exists("input.txt"))
				input = File.ReadAllLines("input.txt").ToList();
			else
				Console.Error.WriteLine("Unable to open input.txt");
		}

		private static List<char> GetDuplicates()
		{
			List<char> repeated = new List<char>();

			foreach (string inp in input)
			{
				string comp1 = inp.Substring(0, inp.Length / 2);
				string comp2 = inp.Substring(inp.Length / 2);

				for (int i = 0; i < comp1.Length; i++)
				{
					if (comp2.Contains(comp1[i]))
					{
						repeated.Add(comp1[i]);
						break;
					}

				}
			}

			return repeated;
		}

		private static List<char> GetBadges()
		{
			List<char> badgeType = new List<char>();

			for (int i = 0; i < input.Count; i++) // Loops through backpack
			{
				for (int j = 0; j < input[i].Length; j++) // Loops through items
				{
					if (input[i + 1].Contains(input[i][j]) && input[i + 2].Contains(input[i][j]))
					{
						badgeType.Add(input[i][j]);
						break;
					}
				}

				i += 2;
			}

			return badgeType;
		}

		private static int CalculateTotal(List<char> items)
		{
			int total = 0;

			foreach (char inp in items)
			{
				total += priorities[inp];
			}

			return total;
		}
	}
}